import * as firebase from 'firebase';


const firebaseConfig = {
  apiKey: "AIzaSyAVCP9cOPkvA7GwqCM-vjkzS9ekifqKAQ0",
  authDomain: "expensify-c7787.firebaseapp.com",
  databaseURL: "https://expensify-c7787.firebaseio.com",
  projectId: "expensify-c7787",
  storageBucket: "expensify-c7787.appspot.com",
  messagingSenderId: "21341205757",
  appId: "1:21341205757:web:f6728ebc8ff5ee73"
};

firebase.initializeApp(firebaseConfig);

const database = firebase.database();

export { firebase, database as default };

  