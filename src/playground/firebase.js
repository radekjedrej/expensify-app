import * as firebase from 'firebase';


const firebaseConfig = {
  apiKey: "AIzaSyAVCP9cOPkvA7GwqCM-vjkzS9ekifqKAQ0",
  authDomain: "expensify-c7787.firebaseapp.com",
  databaseURL: "https://expensify-c7787.firebaseio.com",
  projectId: "expensify-c7787",
  storageBucket: "expensify-c7787.appspot.com",
  messagingSenderId: "21341205757",
  appId: "1:21341205757:web:f6728ebc8ff5ee73"
};

firebase.initializeApp(firebaseConfig);

const database = firebase.database();

const expensesList = database.ref('notes')
  .once('value')
  .then((snapshot) => {
    const expenses = [];

    snapshot.forEach((childSnapshot) => {
      expenses.push({
        id: childSnapshot.key,
        ...childSnapshot.val()
      });
    });
    
    console.log(expenses)
  });

  // database.ref().on('value', (snapshot) => {
  //   const notesData = snapshot.val();
  //   console.log(notesData);

  // });
  
  database.ref('notes').on('value', (snapshot) => {
    const expenses = [];

    snapshot.forEach((childSnapshot) => {
      expenses.push({
        id: childSnapshot.key,
        ...childSnapshot.val()
      });
    });
    
    console.log(expenses)

  }, (e) => {
    console.log('Error with data fetching ', e);
  });
  


  
  // database.ref('notes').push({
//   description: 'Coffe',
//   note: 'Like every morning',
//   amount: 2300,
//   created_at: 120000
// });




// const firebaseNotes = {
//   notes: {
//     aspods: {
//       title: 'First note!',
//       body: 'This is my note'
//     },
//     sadad: {
//       title: 'Second note!',
//       body: 'This is my note'   
//     }
//   }
// };

// const notes = [{
//   id: '12',
//   title: 'First note!',
//   body: 'This is my note'
// },{
//   id: '76122',
//   title: 'Second note!',
//   body: 'This is my note'
// }];

// database.ref('notes').set(firebaseNotes);


// setTimeout(() => {
//   database.ref().update({
//     name: 'Radek Jedrzej'
//   });
// }, 3000)



// const onValueChanged = database.ref().on('value', (snapshot) => {
//   console.log(snapshot.val())
// }, (e) => {
//   console.log('Error with data fetching', e);
// });

// setTimeout(() => {
//   database.ref('age').set(22);
// }, 3500);


// setTimeout(() => {
//   database.ref().off(onValueChanged);
// }, 7000);

// setTimeout(() => {
//   database.ref('age').set(32);
// }, 10500);

// database.ref('location/city')
//   .once('value')
//   .then((snapshot) => {
//     const val = snapshot.val();
//     console.log(val);
//   })
//   .catch((e) => {
//     console.log('Error fetching data', e)
//   });

// database.ref().set({
//   name: 'Radek Jedrej',
//   age: 32,
//   isSingle: true,
//   stressLevel: 6,
//   job: {
//     title: 'Software developer',
//     company: 'Google'
//   },
//   location: {
//     city: 'Bristol',
//     country: 'United Kingdom'
//   }
// }).then(() => {
//   console.log('Data is saved');
// }).catch((e) => {
//   console.log('this failed', e);
// });
      
// database.ref().update({
//   name: 'Edyta Zelma',
//   age: 29,
//   job: 'Software developer',
//   isSingle: null
// });

// database.ref().update({
//   age: 26,
//   'location/city': 'Rynek'
// });

// database.ref().update({
//   'stressLevel' : 10,
//   'job/company': 'Amazon',
//   'location/city': 'Seatle'
// })
  
  // database.ref('isSingle')
  //   .remove()
  //   .then(() => {
  //     console.log('Is single been removed');
  //   }).catch((e) => {
  //     console.log('Error something went wrong', e);
  //   });